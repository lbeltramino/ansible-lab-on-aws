variable "env" {
  description = "Map containing all the environment configuration"
  type        = map(string)
  default     = {}
}

variable "ec2" {
  description = "Map containing all attributes of the Ec2 Instance"
  type        = map(string)
}

