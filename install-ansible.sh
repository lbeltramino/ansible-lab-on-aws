#!/bin/bash
# Update packages, install Ansible and requirements
sudo yum update -y
# Update Session Manager and install plugin
sudo yum install -y https://s3.us-east-1.amazonaws.com/amazon-ssm-us-east-1/latest/linux_amd64/amazon-ssm-agent.rpm
sudo yum install -y https://s3.amazonaws.com/session-manager-downloads/plugin/latest/linux_64bit/session-manager-plugin.rpm
sudo ln -s /usr/local/bin/session-manager-plugin /usr/local/sessionmanagerplugin/bin/session-manager-plugin

# Install Ansible with PIP (latest version)
pip3 install ansible

# Allows Ansible to get inventory hosts from Amazon Web Services EC2.
ansible-galaxy collection install amazon.aws
pip3 install boto3

# This connection plugin allows Ansible to execute tasks on an EC2 instance via the aws ssm CLI.
ansible-galaxy collection install community.aws

# Ansible Bootstrap
EHOME="/home/ec2-user"
echo ${private_key} > $EHOME/.ssh/id_rsa
wget -c https://gitlab.com/naguer/ansible-lab-on-aws/-/archive/main/ansible-lab-on-aws-main.tar.gz -O - | tar -xz -C /home/ec2-user
chown -R ec2-user: $EHOME
cd $EHOME/ansible-lab-on-aws-main && ansible-playbook bootstrap-ansible.yaml
